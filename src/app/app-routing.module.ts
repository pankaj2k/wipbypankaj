import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CoursesComponent } from './courses/courses.component';
import { FooterComponent } from './footer/footer.component';
import { MapLocationComponent } from './map-location/map-location.component';
import { FlagshipProgramComponent } from './flagship-program/flagship-program.component';
import { UltraBitProgramComponent } from './ultra-bit-program/ultra-bit-program.component';
import { TestSeriesComponent } from './test-series/test-series.component';
import { CounsellingComponent } from './counselling/counselling.component';
import { AdvanceCoursesComponent } from './advance-courses/advance-courses.component';
import { OurTeamComponent } from './our-team/our-team.component';
import { CONTACTUSComponent } from './contactus/contactus.component';
import { ModelBlockComponent } from './model-block/model-block.component';
import { EducationOpportunitySmallComponent } from './education-opportunity-small/education-opportunity-small.component';
import { MobileMenuComponent } from './mobile-menu/mobile-menu.component';


const routes: Routes = [
  { path: "", component: HomeComponent},
  { path: 'courses', component: CoursesComponent},

  { path: 'footer', component: FooterComponent},
  { path: 'map-location', component: MapLocationComponent},
  { path: 'flagship-program', component: FlagshipProgramComponent},
  { path: 'ultra-bit-program', component: UltraBitProgramComponent},
  { path: 'test-series', component: TestSeriesComponent},
  { path: 'counselling', component: CounsellingComponent},
  { path: 'advance-courses', component: AdvanceCoursesComponent},
  { path: 'our-team', component: OurTeamComponent},
  { path: 'contactus', component: CONTACTUSComponent},
  { path: 'model-block', component: ModelBlockComponent},
  { path: 'education-opportunity-small', component: EducationOpportunitySmallComponent},
  { path: 'mobile-menu', component: MobileMenuComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ HomeComponent, CoursesComponent,
   FooterComponent, MapLocationComponent, FlagshipProgramComponent,
   UltraBitProgramComponent, TestSeriesComponent, CounsellingComponent,
   AdvanceCoursesComponent, OurTeamComponent, CONTACTUSComponent,
   ModelBlockComponent, EducationOpportunitySmallComponent, MobileMenuComponent
  ]