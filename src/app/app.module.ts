import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ServersComponent } from './servers/servers.component';
import { HeaderComponent } from './header/header.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { MatTabsModule } from '@angular/material/tabs';
import { IgxTabsModule } from 'igniteui-angular';
import { TabsComponent } from './tabs/tabs.component';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { NgxPopper } from 'angular-popper';
import { UserComponent } from './user/user.component';
//import { ModalComponent } from './modal/modal.component';
//import { ModalComponent } from './modal/modal.component';
//import { NgbModule, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import {ModalModule} from "ng2-modal";
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { ModelComponent } from './model/model.component';
import { Model1Component } from './model1/model1.component';
//import { FooterComponent } from './footer/footer.component';
//import { MapLocationComponent } from './map-location/map-location.component';
//import { FlagshipProgramComponent } from './flagship-program/flagship-program.component';
//import { UltraBitProgramComponent } from './ultra-bit-program/ultra-bit-program.component';
//import { TestSeriesComponent } from './test-series/test-series.component';
//import { CounsellingComponent } from './counselling/counselling.component';
//import { AdvanceCoursesComponent } from './advance-courses/advance-courses.component';
//import { OurTeamComponent } from './our-team/our-team.component';
//import { CONTACTUSComponent } from './contactus/contactus.component';
//import { ModelBlockComponent } from './model-block/model-block.component';
//import { EducationOpportunitySmallComponent } from './education-opportunity-small/education-opportunity-small.component';
//import { MobileMenuComponent } from './mobile-menu/mobile-menu.component';
//import { HomeComponent } from './home/home.component';
//import { CoursesComponent } from './courses/courses.component';




@NgModule({
  declarations: [
    AppComponent,
    ServersComponent,
    HeaderComponent,
    TabsComponent,
    UserComponent,
    routingComponents,
    ModelComponent,
    Model1Component,
    //FooterComponent,
    //MapLocationComponent,
    //FlagshipProgramComponent,
    //UltraBitProgramComponent,
    //TestSeriesComponent,
    //CounsellingComponent,
    //AdvanceCoursesComponent,
    //OurTeamComponent,
    //CONTACTUSComponent,
    //ModelBlockComponent,
    //EducationOpportunitySmallComponent,
    //MobileMenuComponent,
    //HomeComponent,
    //CoursesComponent,
    //ModalComponent
    //ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    SlickCarouselModule,
    MatTabsModule,
    IgxTabsModule,
    NgxPopper,
    //ModalModule,
    //NgbModule.forRoot()
    //NgbModule
    //NgbModalConfig
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
