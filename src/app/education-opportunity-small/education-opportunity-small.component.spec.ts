import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationOpportunitySmallComponent } from './education-opportunity-small.component';

describe('EducationOpportunitySmallComponent', () => {
  let component: EducationOpportunitySmallComponent;
  let fixture: ComponentFixture<EducationOpportunitySmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationOpportunitySmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationOpportunitySmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
