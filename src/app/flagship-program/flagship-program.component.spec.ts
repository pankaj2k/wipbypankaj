import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlagshipProgramComponent } from './flagship-program.component';

describe('FlagshipProgramComponent', () => {
  let component: FlagshipProgramComponent;
  let fixture: ComponentFixture<FlagshipProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlagshipProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlagshipProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
