import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UltraBitProgramComponent } from './ultra-bit-program.component';

describe('UltraBitProgramComponent', () => {
  let component: UltraBitProgramComponent;
  let fixture: ComponentFixture<UltraBitProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UltraBitProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UltraBitProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
