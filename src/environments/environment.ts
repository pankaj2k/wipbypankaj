// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  /*
  production: false,
  baseUrl: 'http://localhost/7classes/'
  */
 production: false,
 firebase: {
   apiKey: "AIzaSyAjvljaHHjRNBuG_nhw6iAcNTZ7N8H2DVo",
   authDomain: "student-4ad86.firebaseapp.com",
   databaseURL: "https://student-4ad86.firebaseio.com",
   projectId: "student-4ad86",
   storageBucket: "student-4ad86.appspot.com",
   messagingSenderId: "1007761161875"
 }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
